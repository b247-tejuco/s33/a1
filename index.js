//Part 1

fetch('https://jsonplaceholder.typicode.com/todos/')
	.then((response)=> response.json())

//Part 2

	.then((data)=> {

			let dataMap= data.map(function(toDoList){
			return toDoList.title
		})
			console.log(dataMap)
	})

//Part 3

fetch('https://jsonplaceholder.typicode.com/todos/1')
	.then((response)=> response.json())

//Part 4
	.then((data)=> {
		console.log(data)
		console.log(`The item "${data.title}" on the list has a status of ${data.completed}`)
	})

//Part 5

fetch('https://jsonplaceholder.typicode.com/todos', {
		method: 'POST',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Created To Do List Item',
			completed: false,
			userId: 1
		})
	})
	.then((response)=> response.json())
	.then((data)=> console.log(data))


//Part 6

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PUT',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'Updated To Do List Item',
			dateCompleted: 'Pending',
			description: 'To update my to do list with a different data structure',
			status: 'Pending',
			userId: 1
			})
	})
	.then((response)=>response.json())
	.then((data)=> console.log(data))

// Part 7

fetch('https://jsonplaceholder.typicode.com/todos/1', {
		method: 'PATCH',
		headers: {
			'Content-Type': 'application/json'
		},
		body: JSON.stringify({
			title: 'delectus aut autem',
			dateCompleted: '07/09/21',
			description: 'To update my to do list with a different data structure',
			status: 'Complete',
			userId: 1,
			completed: false
			})
	})
	.then((response)=>response.json())
	.then((data)=> console.log(data))

// Part 8

fetch('https://jsonplaceholder.typicode.com/todos/1', {
				method: 'DELETE'
			})